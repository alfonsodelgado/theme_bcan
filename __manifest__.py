{
    "name": "ThemeBcan",
    "description": "Theme para bases de datos con bcan",
    "version": "0.1",
    "author": "EPII",
    "category": "Theme/Backend",
    "data": [
        "views/assets.xml",
        "views/web.xml",
        "data/ir_config_parameter.xml",
        "templates/webclient_templates.xml",
        "templates/website_templates.xml",
    ],
    "images":[
        "images/screen.png"
    ],
    "depends": [
        "web"
    ],
    "auto_install": False,
    "installable": True,
    "qweb": [
        "static/src/xml/web.xml",
    ],
}